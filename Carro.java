public class Carro {

    private String quantidadePortas;
    private String cor;
    private String quantidadeCalotas;
    private String numeroChassi;
    private String anoFabricacao;
    private String combusitvel;
    private String modelo;


    public Carro(String quantidadePortas, String cor, String quantidadeCalotas, String numeroChassi, String anoFabricação, String combustivel, String modelo) {
        this.quantidadePortas = quantidadePortas;
        this.cor = cor;
        this.quantidadeCalotas = quantidadeCalotas;
        this.numeroChassi = numeroChassi;
        this.anoFabricacao = anoFabricação;
        this.combusitvel = combustivel;
        this.modelo = modelo;
    }

    public String getQuantidadePortas() {
        return quantidadePortas;
    }

    public void setQuantidadePortas(String quantidadePortas) {
        this.quantidadePortas = quantidadePortas;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;

    }
    public String getQuantidadeCalotas() {
        return quantidadeCalotas;
    }
    public void setQuantidadeCalotas(String quantidadeCalotas) {
        this.quantidadeCalotas = quantidadeCalotas;
    }
    public String getnumeroChassi() {
        return numeroChassi;
    }
    public void setnumeroChassi(Integer numerochassi) {
        this.numeroChassi = numeroChassi;
    }
    public String getanoFabricacao() {
        return anoFabricacao;
    }
    public void setAnoFabricacao(String anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }
     public String getCombusitvel() {
        return combusitvel;
    }
    public void setCombusitvel(Integer Combusitvel) {
        this.combusitvel = combusitvel;
    } public String getModelo() {
    return modelo;
    } public void setModelo(Integer Modelo) {
    this.modelo = modelo;

}

    @Override
    public String toString() {
        return "Carro{" +
                "quantidadePortas='" + quantidadePortas + '\'' +
                ", cor='" + cor + '\'' +
                ", quantidadeCalotas='" + quantidadeCalotas + '\'' +
                ", numeroChassi='" + numeroChassi + '\'' +
                ", anoFabricacao='" + anoFabricacao + '\'' +
                ", combusitvel='" + combusitvel + '\'' +
                ", modelo='" + modelo + '\'' +
                '}';
    }
}

